<?php

use Laravel\Lumen\Testing\DatabaseMigrations;

class RolesTest extends TestCase
{
    use DatabaseMigrations;

    /**
     * Test response status
     *
     * @return void
     */
    public function test_list_roles_response_status()
    {
        $this->get('/api/v1/roles');

        $this->assertEquals(
            200,
            $this->response->getStatusCode()
        );
    }

    public function test_list_roles()
    {
        $roles = App\Models\Role::all()->toArray();
        $this->get('/api/v1/roles')->seeJsonEquals($roles);
    }
}
