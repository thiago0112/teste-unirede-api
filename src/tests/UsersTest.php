<?php

use Laravel\Lumen\Testing\DatabaseMigrations;
use Laravel\Lumen\Testing\DatabaseTransactions;

class UsersTest extends TestCase
{
    use DatabaseMigrations;
    use DatabaseTransactions;

    public function test_list_users()
    {
        $users = factory(App\Models\User::class, 1)->create();

        $json = $users->map(function ($user) {
            return [
                'id' => (int) $user->id,
                'login' => $user->login,
                'password' => $user->password,
                'role_id' => (string) $user->role_id,
                'created_at' => (string) $user->created_at,
                'updated_at' => (string) $user->updated_at,
                'role' => $user->role->toArray()
            ];
        })->toArray();

        $this->get('/api/v1/users')->seeJsonEquals($json);
    }

    public function test_create_user()
    {
        $user = factory(App\Models\User::class)->make();

        $this->post('/api/v1/users', $user->toArray());

        $this->seeInDatabase('users', $user->toArray());

        $this->assertResponseStatus(200);
    }

    public function test_create_user_requires_data()
    {
        $this->post('/api/v1/users', []);

        $this->assertResponseStatus(422);
    }

    public function test_create_duplicated_user_should_fail()
    {
        $user = factory(App\Models\User::class)->make();

        factory(App\Models\User::class)->create([
            'login' => $user->login
        ]);

        $this->post('/api/v1/users', $user->toArray());

        $this->assertResponseStatus(422);
    }

    public function test_update_user()
    {
        $user = factory(App\Models\User::class)->create();
        $user->login = 'new login';

        $this->put("/api/v1/users/{$user->id}", $user->toArray());


        $this->assertResponseStatus(200);
        $this->seeInDatabase('users', $user->toArray());
    }

    public function test_update_user_login_ignores_ifself()
    {
        $user = factory(App\Models\User::class)->create();
        $user->password = 'new pass';

        $this->put("/api/v1/users/{$user->id}", $user->toArray());


        $this->assertResponseStatus(200);
        $this->seeInDatabase('users', $user->toArray());
    }

    public function test_update_invalid_user()
    {
        $user = factory(App\Models\User::class)->make();

        $this->put("/api/v1/users/1", $user->toArray());

        $this->assertResponseStatus(404);
    }

    public function test_update_user_requires_data()
    {
        $user = factory(App\Models\User::class)->create();

        $this->put("/api/v1/users/{$user->id}", []);

        $this->assertResponseStatus(422);
    }

    public function test_remove_user()
    {
        $user = factory(App\Models\User::class)->create();

        $this->delete("/api/v1/users/{$user->id}");

        $this->assertResponseStatus(200);
        $this->assertEquals(0, App\Models\User::count());
    }

    public function test_remove_invalid_user()
    {
        $this->delete("/api/v1/users/1");

        $this->assertResponseStatus(404);
    }

    public function test_get_user()
    {
        $user = factory(App\Models\User::class)->create();
        $user->role_id = (string) $user->role_id;

        $this->get("/api/v1/users/{$user->id}")->seeJsonEquals($user->toArray());
    }

    public function test_get_invalid_user()
    {
        $this->get("/api/v1/users/1");

        $this->assertResponseStatus(404);
    }
}
