<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Role model
 * @author Thiago Silva <thiago.silva@deliverit.com.br>
 */
class Role extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * Users items relation
     */
    public function users()
    {
        return $this->hasMany(User::class);
    }
}

