<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * User model
 * @author Thiago Silva <thiago.silva@deliverit.com.br>
 */
class User extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'login', 'password', 'role_id'
    ];

    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = true;

    /**
     * User role relation
     */
    public function role()
    {
        return $this->belongsTo(Role::class);
    }
}

