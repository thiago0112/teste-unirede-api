<?php

namespace App\Http\Controllers;

use App\Models\Role;

/**
 * Roles controller
 * @author Thiago Silva <thiago0112@gmail.com>
 */
class RolesController extends Controller
{
    /**
     * List roles in json
     *
     * @return JsonResponse
     */
    public function list()
    {
        return Role::all();
    }
}
