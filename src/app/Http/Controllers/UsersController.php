<?php

namespace App\Http\Controllers;

use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

/**
 * Users controller
 * @author Thiago Silva <thiago0112@gmail.com>
 */
class UsersController extends Controller
{
    /**
     * Validation rules
     *
     * @var array
     */
    protected $rules = [
        'login' => 'required|unique:users',
        'password' => 'required',
        'role_id' => 'required'
    ];

    /**
     * List users in json
     *
     * @return Collection
     */
    public function list()
    {
        return User::with('role')->get();
    }

    /**
     * Get user
     *
     * @return User
     */
    public function show($id)
    {
        return User::findOrFail($id);
    }

    /**
     * Create user
     *
     * @param Request $request
     * @return User
     */
    public function create(Request $request)
    {
        $this->validate($request, $this->rules);

        $user = new User();

        $user->fill($request->all());

        $user->save();

        return $user;
    }

    /**
     * Update user
     *
     * @param int $id
     * @param Request $request
     * @return User
     */
    public function update($id, Request $request)
    {
        $this->validate($request, array_merge($this->rules, [
            'login' => [
                'required',
                Rule::unique('users')->ignore($id, 'id')
            ]
        ]));

        $user = User::findOrFail($id);

        $user->fill($request->all());
        $user->save();

        return $user;
    }

    /**
     * Update user
     *
     * @param int $id
     * @return array
     */
    public function delete($id)
    {
        $user = User::findOrFail($id);

        $user->delete();

        return ['deleted' => true];
    }
}
