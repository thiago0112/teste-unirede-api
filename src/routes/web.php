<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$router->get('/', function () use ($router) {
    return $router->app->version();
});

$router->group(['prefix' => 'api/v1'], function()  use ($router) {
    $router->get('roles', 'RolesController@list');

    $router->get('users', 'UsersController@list');
    $router->get( 'users/{id:[0-9]+}', 'UsersController@show');
    $router->post('users', 'UsersController@create');
    $router->put('users/{id:[0-9]+}', 'UsersController@update');
    $router->delete('users/{id:[0-9]+}', 'UsersController@delete');
});
