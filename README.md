
#  Teste Unirede API



Projeto desenvolvido para o teste de desenvolvedor(a) da UNIREDE



##  Executando o projeto
1. Clonar o repositório
2. Acessar a pasta onde o repositório foi clonado
3. Montar o ambiente docker
	`# docker-compose up`
4. Acessar o container criado
	`# docker exec -it unirede-php bash`
5. Instalar as dependências do composer
	`# composer install`
6. Copiar o arquivo de configurações do projeto
	`# cp .env.example .env`
## Executando os testes
1. Acessar o container criado
	`# docker exec -it unirede-php bash`
2. Executar o PHPUnit
	`# ./vendor/bin/phpunit`